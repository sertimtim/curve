#include <iostream>

#include "vec.h"
#include <memory>

class Curve
{
public:
    enum CurveType
    {
        CT_Undefined = 0,
        CT_Line,
        CT_Circle
    };

    Curve( CurveType type ) : m_type( type ) {}
    CurveType getCurveType() const { return m_type; }
    virtual VecD3 pointOn( double t ) const = 0;
    virtual ~Curve() {}

private:
    const CurveType m_type;
};

class Curve_Line : public Curve
{
public:
    Curve_Line(const VecD3& point, const VecD3& dir)
        : Curve( CT_Line )
        , m_point( point )
        , m_dir( dir )
    {

    }
    //virtual CurveType getCurveType() const override { return CT_Line; }

    virtual VecD3 pointOn( double t ) const override
    {
        return m_point + ( m_dir * t );
    }
    const VecD3& dir() const {return m_dir;}
    const VecD3& point() const {return m_point;}

private:
    VecD3 m_point;
    VecD3 m_dir;
};

class Curve_Circle : public Curve
{
public:
    Curve_Circle(const VecD3& point, const VecD3& axis, const VecD3& ref_dir, double radius)
        : Curve( CT_Circle )
        , m_point( point )
        , m_axis( axis )
        , m_ref_dir( ref_dir )
        , m_rot_dir( m_axis.vecProduct(m_ref_dir) )
        , m_radius( radius )
    {

    }
    //virtual CurveType getCurveType() const override { return CT_Circle; }

    virtual VecD3 pointOn( double t ) const override
    {
        return m_point + ( ( ( m_ref_dir * std::cos( t ) ) + ( m_rot_dir * std::sin( t ) ) ) * m_radius );
    }

private:
    VecD3 m_point;
    VecD3 m_axis;
    VecD3 m_ref_dir;
    VecD3 m_rot_dir;
    double m_radius;
};

void intersectLineLine( const Curve_Line* curve1, const Curve_Line* curve2 )
{
    VecD3 c;    // Нормальный вектор
    VecD3::vecProduct(curve1->dir(), curve2->dir(), c);
    double p, n;
    p = - (c * curve2->point()); // Коэфициент в уравнении плоскости
    n = VecD3::normFactor(&c);  // Нормирующий множитель
    if(p<0)
    {
        c = c*n;
        p = p*n;
    }
    else
    {
        c = c*(-n);
        p = p*(-n);
    }
    double l;
    l = fabs(c * curve1->point() + p);
    std::cout << l;
}

void intersectLineCircle( const Curve_Line* line, const Curve_Circle* circle)
{

}

void intersect( const Curve* curve1, const Curve* curve2)
{
    if ( curve1->getCurveType() == Curve::CT_Line && curve2->getCurveType() == Curve::CT_Line )
        intersectLineLine( static_cast<const Curve_Line*>( curve1 ), static_cast<const Curve_Line*>( curve2 ) );
    else if ( curve1->getCurveType() == Curve::CT_Line && curve2->getCurveType() == Curve::CT_Circle )
        intersectLineCircle( static_cast<const Curve_Line*>( curve1 ), static_cast<const Curve_Circle*>( curve2 ) );
    else if ( curve1->getCurveType() == Curve::CT_Circle && curve2->getCurveType() == Curve::CT_Line )
        intersectLineCircle( static_cast<const Curve_Line*>( curve2 ), static_cast<const Curve_Circle*>( curve1 ) );
}

using CurvePtr = std::shared_ptr<Curve>;

int main()
{
    CurvePtr curve1, curve2;

    curve1.reset( new Curve_Line(VecD3(-2, 1, 4), VecD3(0, 2, -3)) );
    curve2.reset( new Curve_Line(VecD3(0, 1, -4), VecD3(1,-2, 6)) );
    intersect(curve1.get(), curve2.get());

    curve1.reset( new Curve_Line(VecD3(0, 0, 0), VecD3(1, 0, 0)) );
    curve2.reset( new Curve_Circle(VecD3(0, 0, 0), VecD3(1,0, 0), VecD3(0, 1, 0), 1.0) );
    intersect(curve1.get(), curve2.get());


    VecD3 p1 = curve1->pointOn( M_PI );
    VecD3 p2 = curve2->pointOn( M_PI );

    return 0;
}
