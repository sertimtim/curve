#ifndef VEC_H
#define VEC_H
#include <array>
#include <cmath>

template<typename T, size_t N>
class Vec
{
private:

    std::array<T, N> v;
public:
    Vec()
    {
        for(size_t i=0; i<v.size(); i++)
            v[i] = 0;
    }

    Vec(T x, T y, T z)
    {
        v[0] = x;
        v[1] = y;
        v[2] = z;
    }

    static void vecProduct(const Vec& a, const Vec& b, Vec& c)
    {
        c.v[0] = a.v[1] * b.v[2] - a.v[2] * b.v[1];
        c.v[1] = a.v[2] * b.v[0] - a.v[0] * b.v[2];
        c.v[2] = a.v[0] * b.v[1] - a.v[1] * b.v[0];
    }

    static T normFactor(const Vec* c)
    {
        T n;
        n = 1 / (sqrt( (c->v[0] * c->v[0]) + (c->v[1] * c->v[1]) + (c->v[2] * c->v[2]) ));
        return n;
    }

    Vec vecProduct(const Vec& b) const
    {
        Vec c;
        c.v[0] = v[1] * b.v[2] - v[2] * b.v[1];
        c.v[1] = v[2] * b.v[0] - v[0] * b.v[2];
        c.v[2] = v[0] * b.v[1] - v[1] * b.v[0];
        return c;
    }

    Vec operator+(const Vec& ob) const
    {
        Vec temp;
        for(int i=0; i<N; i++)
            temp.v[i] = v[i] + ob.v[i];
        return temp;
    }

    Vec operator*(const T& t) const
    {
        Vec temp;
        for(int i=0; i<N; i++)
            temp.v[i] = v[i] * t;
        return temp;
    }

    T operator*(const Vec& ob) const
    {
        T sum;
        for(int i=0; i<N; i++)
            sum += v[i] * ob.v[i];
        return sum;
    }

    Vec operator/(const T& n) const
    {
        Vec temp;
        for(int i=0; i<N; i++)
            temp.v[i] = v[i] / n;
        return temp;
    }
};


template<typename T> using Vec3 = Vec<T,3>;

using VecD3 = Vec3<double>;

#endif // VEC_H
